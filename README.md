# Crear repositorio en GitLab

## Crear repositorio
- Ir a GitLab [https://gitlab.com/](https://gitlab.com/) loguearse y en la página principal (projects) clicar en el botón "**New Project**"
- Poner nombre del proyecto (*ej: mod2-exercises*) y poner la visivilidad del repositorio como "**public**". Clicar en "**Create Project**"

## Clonar repositorio
- En la siguiente página, arriba a la derecha, en el boton de "**clone**" copiar la primera url ("*Clone with SSH*")
- Abrir la consola e ir al directorio donde se quiera clonar el repositorio (*ej: Documentos*). No hace falta hacer una carpeta nueva porque al clonar ya va a crear la carpeta con el nombre del repositorio (*ej: Documents/mod2-exercises*)
- poner en la consola `git clone URL` donde URL es la url que acabamos de copiar y darle a enter. Eso clona el repositorio y crea la carpeta donde ya está Git iniciado (no hace falta hacer `git init`).

## Añadir y subir documentos
- En esa carpeta meter los documentos que se quieran subir a GitLab
- En la consola, entrar en la carpeta que se creó al hacer el clone (*ej: Documents/mod2-exercises*) y escribir: 
    - `git add .` ===> mete todos los archivos nuevos o con cambios en el stage (preparados para meter en un commit) 
    - `git commit -m 'nombre que se quiera poner al commit'` ===>  Crea un commit con todos los archivos que haya en el stage
    - `git push origin master` ===> sube los commits a GitHub en la rama master (la primera vez como master no existe también crea la rama automaticamente)

#### NOTA:
Cada vez que se quiera subir nuevos archivos o modificaciones seguir los pasos del último punto (**Añadir y subir archivos**)